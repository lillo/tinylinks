open Syntax
open Env
open Val_dom

module Correspondence =
struct
  open Tipo

  type cor = (tipo * pred) list

  let empty_cor = ([]: cor)

  let make_cor avar preds :cor = 
    List.map (fun x -> (avar, x)) preds

  let reduce_cor var c = 
    let fold_fun acc el = 
      if fst(el) = var then
	snd(el)::acc
      else
	acc
    in
      List.fold_left fold_fun [] c

  let filter_cor var c = 
    let fold_fun acc el = 
      if fst(el) = var then
	acc
      else
	el::acc
    in
      List.fold_left fold_fun [] c

  let codomain c = 
    snd(List.split c)

  let apply_subst_cor subst c =
    List.map (fun (var,p) -> (apply_subst subst var, p)) c

  let apply_subst_cor_list subst c_list =
    List.map (apply_subst_cor subst) c_list

  let combine_cor c1 c2 =
    let (_,n1) = List.partition (fun x -> List.mem x c2) c1 in
      n1 @ c2

  let combine_cor_list c_list =
    List.fold_left combine_cor empty_cor c_list

  let new_cor subst c_list =
    combine_cor_list (apply_subst_cor_list subst c_list)

  let ann_printer  =
    let bind_printer ppf = function
        (t,p) -> Format.fprintf ppf"(%a,%s)" tipo_printer t p
    in
      Myformat.list_printer (bind_printer, "[","]",",")
end

module Fun = 
struct 
  exception Undef_fun of string

  type ('a,'b) t = ('a * 'b) list

  let undef_fun =
    ([] : ('a,'b) t)

  let add_point (f, x, y) = 
    (x,y) :: (List.remove_assoc x f)

  let apply (f, x) =
    try
      List.assoc x f
    with Not_found -> raise (Undef_fun("Fun.apply: function is undefined"))

  let printer (a_printer, b_printer) = 
    let bind_printer ppf (a,b) = 
      Format.fprintf ppf "%a -> %a" 
	a_printer a b_printer b 
    in
      Myformat.list_printer(bind_printer, "{", "}", ",")

  let merge comb_fun f1 f2 = 
    let (c,n) = List.partition (fun (x,_) -> List.exists (fun (x',_) -> x = x') f2) f1 in
    let map_fun (x,y) = 
      try
	let y' = apply (c,x) in
	  (x, comb_fun y y')
      with Undef_fun(_) ->
	(x,y)
    in
      n @ (List.map map_fun f2)


  let ( |>> ) f ocaml_fun = 
    List.map (fun (x,y) -> (x,ocaml_fun y)) f

  let ( |<- ) f ndom = 
    let map_fun x = 
      try
	(x, apply(f,x))
      with Undef_fun(_) ->
	failwith "fun_reduce: function undef on one point"
    in
      List.map map_fun ndom

  let dom f = 
    fst (List.split f)

  let extract_fun p1 p2 f1 f2 = 
    let filter_fun (x,y) = 
      try
	let y' = apply(f2, x) in
	  p2(y,y')
      with Undef_fun(_) ->
	p1(y)
    in
      List.filter filter_fun f1

  let exists p f = 
    List.exists (fun (_,y) -> p y) f

  let fun_printer bind_printer = 
    Myformat.list_printer (bind_printer, "{", "}", ",")
	
end


module AbstractDomain : VALUES= 
  functor (Env:ENV) ->
struct
  open Tipo
  open Fun
  open Correspondence

  exception No_type of string

  type dval = 
      Dint of int
    | Var of tipo
    | No_dval
    | Unknown
    | Dval

  type mark = E | A | EA

  type eenv = (pred, dval * mark) Fun.t
  type tpred = (pred, dval) Fun.t

  type eval = tipo * subst * dval * cor * tpred

  type env = (ide, eval) Env.t

  type sem_val_fun = value -> env -> eenv -> eval
  type sem_exp_fun = exp -> env -> eenv -> (eval * eenv)

  let empty_eenv : eenv = Fun.undef_fun


  let string_of_dval = function
      Dint(n) -> string_of_int n
    | Var(ParFormal(v)) -> v
    | No_dval -> "No_dval"
    | Unknown -> "Unknown"
    | Dval -> "Dval"
    | _ -> failwith "string_of_dval: unacceptable case" 

  let string_of_mark = function
      A -> "A"
    | E -> "E"
    | EA -> "EA"

  let combine_dval dv1 dv2 = 
    match (dv1, dv2) with
	(Dval,_) 
      | (_,Dval) -> Dval
      |	(Unknown, _) -> dv2
      | (_, Unknown) -> dv1
      | (d1,d2) when d1 = d2 -> d1
      | _ ->  Dval (* failwith "combine_dval: incompatible dval -> lub fail" *)

  let apply_subst_tpred (sub:subst) (f : tpred) :tpred = 
    let f_aux = function
	Var(t) -> Var(apply_subst sub t)
      | dv -> dv
    in
      f |>> f_aux

  let apply_subst_tpred_list sub =
    List.map (apply_subst_tpred sub) 
    
  let combine_tpred : tpred -> tpred -> tpred = 
    let com_fun d1 d2 = 
      match (combine_dval d1 d2) with
	  Dint(_)
	| Var(_) as d -> d
	| d -> failwith ("combine_tpred: invalid dval = " ^ (string_of_dval d))
    in
      merge com_fun
    
  let combine_tpred_list = 
    List.fold_left combine_tpred undef_fun 

  let new_tpred sub fl = 
    combine_tpred_list (apply_subst_tpred_list sub fl)

  let apply_subst_eenv (sub:subst) (phi: eenv) : eenv = 
    let f_aux = function
	(Var(t),m) -> (Var(apply_subst sub t),m)
      | c -> c
    in
      phi |>> f_aux

  let apply_subst_eenv_list sub = 
    List.map (apply_subst_eenv sub)

  let combine_eenv : eenv -> eenv -> eenv = 
    let com_fun v1 v2 = 
      match (v1,v2) with
	  ((d, m),(d',m')) when m = m' -> 
	    begin 
	      match (combine_dval d d') with
		  Dint(_)
		| Var(_) as dv -> (dv, m)
		| d -> failwith ("combine_eenv: invalid dval = " ^ (string_of_dval d))
	    end
	| _ -> failwith "combine_eenv: impossible combine eenv"
    in
      merge com_fun

  let combine_eenv_list = 
    List.fold_left combine_eenv undef_fun

  let new_eenv sub phil = 
    combine_eenv_list (apply_subst_eenv_list sub phil)

  let tpred_to_eenv ((f,m):tpred * mark) = 
    f |>> (fun x -> (x,m))

  let eenv_to_tpred (phi:eenv) : tpred = 
    phi |>> fst
    
  let extract_eenv (var_ann, c, f, m) = 
    let ndom = reduce_cor var_ann c in
      (f |<- ndom) |>> (fun x -> (x,m))

  let subst_var_eenv var dv phi = 
    let f_aux (d,m) = 
      match d with 
	  Var(v) when v = var -> (dv,m)
	| _ -> (d,m)
    in
      phi |>> f_aux

  let subst_var_tpred var dv f = 
    let f_aux d = 
      match d with
	  Var(v) when v = var -> dv
	| _ -> d
    in
      f |>> f_aux

  let get_assert : eenv -> eenv -> eenv = 
    let p1 v = snd(v) = A in
    let p2 (v,v') = (snd(v) = A) && (snd(v') = E) in
      extract_fun p1 p2

  let get_event : eenv -> eenv-> eenv = 
    let p1 v = snd(v) = E in
    let p2 = fun x -> false
    in
      extract_fun p1 p2

  let get_diff :eenv -> eenv -> eenv = 
    let p1 = fun x -> true in
    let p2 (v,v') = snd(v) != snd(v') in
      extract_fun p1 p2

  let subeffect_eenv phi1 phi2 = 
    let p_inter (a,b) = fst(a) = fst(b) && snd(b) = snd(a) && snd(b) = E in
    let intersec = extract_fun (fun x -> false) p_inter phi1 phi2 in
    let p_union1 = extract_fun (fun x -> snd(x) = A) (
      fun (a,b) -> snd(a) = snd(b) && snd(b) = A) phi1 phi2 in
    let p_union2 = extract_fun (fun x -> snd(x) = A) (
      fun (a,b) -> snd(a) = snd(b) && snd(b) = A) phi2 phi1 in
      combine_eenv intersec (combine_eenv p_union1 p_union2)

  let cor_from_eenv (avar:tipo) (phi:eenv) = 
    make_cor avar (dom phi)

  let filter_eenv (c:cor) (phi:eenv) = 
    let ccor = codomain c in 
    let ndom = List.filter (fun x -> List.mem x ccor) (dom phi) in
    (phi |<- ndom)

  let verify_incl pre phi = 
    let pre_dom = dom pre in
    let forall_fun x = 
      let (v1,m1) = Fun.apply(pre,x) in
      let (v2,m2) = Fun.apply(phi,x) in
      let t = match (v1,v2) with
	  (Dint(n),Dint(n')) -> n = n'
	| (Var(ParFormal(v)),Var(ParFormal(v'))) -> Printf.printf "%s = %s\n" v  v'; v = v'
	| _ -> false
      in
	t && m1 = A && (m2 = E || m2 = EA) 
    in
      if List.for_all forall_fun pre_dom then
	phi
      else
	failwith "verify_incl: no preconditions"

  let occur_eenv var phi = 
    let a = Var(ParFormal(var)) in
      exists (fun (x,_) -> x = a) phi

  let tunit = Symb("Unit", [])
  let tinteger = Symb("Integer", [])
  let tstring = Symb("String", [])
  let txml va = Symb("Xml", [va])
  let tlink va = Symb("Link", [va])
  let tform va = Symb("Form", [va])
  let ttuple va_list = Symb("Tuple", va_list)
  let tlist vt = Symb("List", [vt])
  let tfunction (var, vt1, va1, vt2,va2) =
    Symb("Function",[var;vt1;va1;vt2;va2])

  let unify_list l s =
    try
      unify_list l
    with Failure(a) ->
      raise (No_type(a ^ ": " ^ s))

  let type_unify str_err type1 type2 = 
    let (t1, s1, dv1, c1, f1) = type1 in
    let (t2, s2, dv2, c2, f2) = type2 in
    let nsub = unify_list ((t1,t2) :: (s1@s2)) str_err in
      (apply_subst nsub t1, 
       nsub, 
       combine_dval dv1 dv2,
       new_cor nsub [c1;c2],
       new_tpred nsub [f1;f2])

  let fresh_type (ty,dv) = 
    (ty,empty_subst,dv,empty_cor, Fun.undef_fun)

  let make_val x = x

  let make_unit = function
      Unit -> fresh_type (tunit, No_dval)
    | _ -> raise (No_type("make_unit: no unit value"))

  let make_int sem (v, r, phi) = 
    match v with
	Zero -> fresh_type (tinteger, Dint(0))
      | Succ(p) -> 
	  let (t,s,dv,c,f) = type_unify "make_int: no integer predecessor"
	    (fresh_type (tinteger, Unknown)) (sem p r phi) 
	  in
	    begin
	      match dv with
		  Dint(n) -> (t, s, Dint(n+1),c,f)
		| _ -> raise (No_type("make_int: no dval integer"))
	    end
      | _ -> raise (No_type("make_int: no integer value"))

  let make_string sem (v,r,phi) = 
    match v with
	String(chars) ->
	  let fold_fun type1 char = 
	    type_unify "make_string: no integer caracter"
	      type1 (sem char r phi) 
	  in
	  let (_,s,_,c,f) = List.fold_left fold_fun 
	    (fresh_type(tinteger, Dval)) chars
	  in
	    (tstring,s,No_dval, c, f)
      | _ -> raise (No_type("make_string: no string value"))

  let make_list sem (v, r, phi) = 
    match v with
	Nil -> fresh_type (tlist (new_typevar()), No_dval)
      | Cons(v1,v2) ->
	  let (t1,s1,dv1,c1,f1) = sem v1 r phi in
	  let tycons = sem v2 r phi in
	    type_unify "make_list: element and list have different types"
	      tycons (tlist(t1),s1,No_dval,c1,f1)
      | _ -> raise (No_type("make_list: no list value"))

  let make_tuple sem (v, r, phi) = 
    match v with
	Tuple(elemens) ->
	  let rec my_split = function
              [] -> ([],[],[],[])
            | ((t,s,_,c,a))::tail ->
                let (l1, l2, l3, l4) = my_split tail in
                  (t::l1, s::l2, c::l3, a::l4)
          in
          let (tys, subs, cs, ts) =
            my_split (List.map (fun x -> sem x r phi) elemens) in
          let nsub = List.fold_left compose empty_subst subs in
          let n_c = new_cor nsub cs in
          let n_f = new_tpred nsub ts in
          let ty = apply_subst nsub (ttuple tys) in
            (ty, nsub, No_dval, n_c, n_f)
      | _ -> raise (No_type("make_tuple: no tuple argument"))

  let make_xml sem (v, r, phi) = 
    match v with 
	Text(s) ->
	  let avar = new_annvar () in
	  let (_,s,_,c,f) = type_unify "make_xml: no string in text element"
	    (fresh_type (tstring,No_dval)) (sem s r phi)
	  in
	    (txml(avar), s, No_dval, c, f)
      | Elem(s, tags) ->
	  let avar = new_annvar () in
	  let (_, s, _, c, f) = type_unify 
	    "make_xml: no string in first elemement of elem" 
	    (sem s r phi) (fresh_type (tstring,No_dval))
	  in
	  let fold_fun type1 v = 
	    let ty = sem v r phi in
	    let avar = new_annvar () in
	    let rec f_aux = function
	      h::t -> begin
		try
		      type_unify "" ty (fresh_type (h,No_dval))
		with No_type(_) -> f_aux t
	        end
	      | [] -> type_unify "make_xml: no xml subtag" type1 ty
	    in
	    let (_,s,_,c,f) = f_aux [tlink(avar); tform(avar)] in
	      (apply_subst s (txml(avar)), s, No_dval, c, f)
	  in
	    List.fold_left fold_fun (txml(avar), s, No_dval, c, f) tags
 
      | _ -> raise (No_type("make_xml: no xml value")) 
	    

  let make_fun (sem:sem_exp_fun) (v, r, (phi:eenv)) = 
    match v with 
	Fun(var, e) ->
	  let ffvar = parformal_from_var(var) in
	  let tvar = new_typevar () in
	  let avar1 = new_annvar () in
	  let avar2 = new_annvar () in
	  let r' = Env.bind(r, var, fresh_type (tvar, Var(ffvar))) in
	  let ((t,s, dv, c, f),phi') = sem e r' phi in
	  let navar1 = apply_subst s avar1 in
	  let navar2 = apply_subst s avar2 in 
	  let sphi = apply_subst_eenv s phi in 
	  let c_pred = cor_from_eenv navar1 (get_assert phi' sphi) in
          let c_post = cor_from_eenv navar2 (get_event phi' sphi) in
          let c_n = new_cor s [c;c_pred;c_post] in
          let f_n = eenv_to_tpred (filter_eenv c_n (get_diff phi' sphi )) in
	  let funty = apply_subst s (tfunction(ffvar, tvar, navar1, t, navar2)) in
	    (funty, s, No_dval, c_n, new_tpred s [f;f_n])
      | _ -> raise (No_type("make_fun: no functional object"))

  let make_fun_rec (sem:sem_exp_fun) (var, v, r, phi) = 
    match v with
	Fun(_) ->
	  let ft = fresh_type (new_typevar(), Unknown) in
	  let r' = Env.bind(r, var, ft) in
	  let t = make_fun sem (v, r', phi) in
	    type_unify "make_fun_rec: no unification" ft t
      | _ -> raise (No_type("make_fun_rec: no functional object"))

  let make_xml_fun (sem:sem_exp_fun) (v, r, phi) = 
    match v with
	Href(e) ->
	  let ((t,s,dv,c,f),phi') = sem e r phi in
	  let phi_assert = get_assert phi'  phi in
          let phi_event = get_event phi' phi in
            if phi_event != empty_eenv then
              raise (No_type("make_xml_fun: exp in href can't generate new events"))
            else
	      let avar = new_annvar () in 
	      let xmlty = txml(avar) in 
	      let nsub = unify_list ((xmlty,t) :: s)
		"make_fun_xml: no xml expression in href"
	      in
	      let c_pred = cor_from_eenv avar phi_assert in
	      let f_n = eenv_to_tpred phi_assert in
		(apply_subst nsub (tlink(avar)), nsub, 
		 No_dval, new_cor nsub [c;c_pred],
		 new_tpred nsub [f;f_n])
		
      | Form(varl, e) ->
	  let tyl = List.map (fun _ -> fresh_type (tstring,No_dval)) varl in
          let r' =
            try
              Env.bind_list(r, varl, tyl)
            with _ -> raise (No_type("make_xml_fun: error in bind_list"))
          in
	  let ((t, s, dv, c, f),phi') = sem e r' phi in
	  let phi_assert = get_assert phi' phi  in
          let phi_event = get_event phi' phi  in
            if phi_event != empty_eenv then
              raise (No_type("make_xml_fun: exp in form can't generate new events"))
            else
	      let avar = new_annvar () in 
	      let xmlty = txml(avar) in 
	      let nsub = unify_list ((xmlty,t) :: s)
		"make_fun_xml: no xml expression in form"
	      in
	      let c_pred = cor_from_eenv avar phi_assert in
	      let f_n = eenv_to_tpred phi_assert in
		(apply_subst nsub (tform(avar)), nsub, 
		 No_dval, new_cor nsub [c;c_pred],
		 new_tpred nsub [f;f_n])  
      | _ -> raise (No_type("make_fun_xml: no "))

  let apply_fun (fv, p, phi) = 
    let (tf, sub1, dv1, c1, f1) = fv in
    let (tp, sub2, dv2, c2, f2) = p in
    let tvar = new_typevar () in
    let ffvar = new_parformal () in
    let avar1 = new_annvar () in 
    let avar2 = new_annvar () in 
    let fn = tfunction(ffvar, tp, avar1, tvar, avar2) in
    let nsub = unify_list ((tf,fn) :: (sub1 @ sub2))
      "apply_fun: no unification "
    in
(*      let nc = apply_subst_cor nsub c1 in
      let nf = apply_subst_tpred nsub f1 in
      let ppre = extract_eenv ((apply_subst nsub avar1),nc,nf, A) in
      let pre = subst_var_eenv (apply_subst nsub ffvar) dv2 ppre in
      let ppost = extract_eenv ((apply_subst nsub avar2),nc,nf, E) in
      let post = subst_var_eenv (apply_subst nsub ffvar) dv2 ppost in
	try
	  let nphi = verify_incl (apply_subst_eenv nsub pre) (apply_subst_eenv nsub phi) in
(*	  let nphi' = subst_var_eenv (apply_subst nsub ffvar) dv2 nphi in*)
	   ((apply_subst nsub tvar, nsub,
	     Unknown,
	     new_cor nsub [nc;c2],
	     new_tpred nsub [nf;f2]),
	    new_eenv nsub [nphi; post]
	   )
*)
    let nc1 = apply_subst_cor nsub c1 in
    let nf = apply_subst_tpred nsub f1 in
    let avar1' = apply_subst nsub avar1 in
    let avar2' = apply_subst nsub avar2 in
    let nf1 = subst_var_tpred (apply_subst nsub ffvar) dv2 nf in
    let pre = extract_eenv (avar1', nc1, nf1, A) in
    let post = extract_eenv (avar2', nc1, nf1, E) in
    let nc = filter_cor avar1' (filter_cor avar2' nc1) in
    let nf' = nf1 |<- (codomain nc) in
      try
	let nphi = verify_incl pre (apply_subst_eenv nsub phi) in
	  ((apply_subst nsub tvar, nsub,
	    Unknown,
	    new_cor nsub [nc;c2],
	    new_tpred nsub [nf';f2]),
	   new_eenv nsub [nphi; post]
	  )
      with _ -> 
	  raise (No_type("apply_fun: no preconditions"))

  let int_op op (n1, n2) =
    let apply_op = function
	(Dint(a),Dint(b)) -> Dint(op a b)
      | (No_dval,_) 
      | (_, No_dval) -> No_dval
      | _ -> Unknown
    in
    let (t1,sub1,dv1,c1,f1) = n1 in
    let (t2,sub2,dv2,c2,f2) = n2 in
    let nsub = unify_list ((tinteger, t1) :: (tinteger,t2) :: (sub1@sub2))
      "int_op: no integer argument"
    in
      (tinteger, nsub,
	apply_op(dv1,dv2),
        new_cor nsub [c1;c2],
        new_tpred nsub [f1;f2])

    let sem_op sem_exp op (e1,e2,r,f) =
      let (v1, f1) = sem_exp e1 r f in
      let (v2, f2) = sem_exp e2 r f1 in
        (int_op op (v1,v2), f2)

    let event_assert event (q,v,phi) = 
      let m = if event then E else A in
      let (t, s, dv, _, _) = v in
      let nsub = unify_list ((tinteger, t) :: s) 
	"event_assert: argument of event is not an integer"
      in
	try
	  let (dv1, m1) = Fun.apply(phi, q) in
	    match (m,m1) with
		(A, A) when dv1 != dv -> 
		  raise (No_type("event_assert: double assertion with different value"))
	      | (E, E) when dv1 != dv ->
		  raise (No_type("event_assert: double event with different value"))
	      | (E, A) -> raise (No_type("event_assert: generation after assertion"))
	      | _ -> 
		  begin
		    match dv with
			Dint(_) -> 
			  if dv = dv1 then
			    ((tunit, nsub, No_dval, empty_cor, Fun.undef_fun),
			     Fun.add_point(phi, q, (dv,m)))
			  else
			    raise (No_type("event_assert: event/assert with different constant value"))
		      | Var(_) -> ((tunit, nsub, No_dval, empty_cor, Fun.undef_fun),
				   Fun.add_point(phi, q, (dv,m)))
		      | _ -> raise (No_type("event_assert: invalid dval"))
		end
	with Undef_fun(_) ->
	  match dv with
	      Dint(_) 
	    | Var(_) -> ((tunit, nsub, No_dval, empty_cor, Fun.undef_fun),
			 Fun.add_point(phi, q, (dv,m)))
	    | _ -> raise (No_type("event_assert: invalid dval"))

(*
      let event_assert event (q,v,phi) = 
      let m = if event then E else A in
      let (t, s, dv, _, _) = v in
      let nsub = unify_list ((tinteger, t) :: s) 
	"event_assert: argument of event is not an integer"
      in
	match dv with
	    Dint(_)
	  | Var(_) -> ((tunit, nsub, No_dval, empty_cor, Fun.undef_fun),
		       Fun.add_point(phi, q, (dv,m)))
	  | _ -> raise (No_type("event_assert: invalid dval"))
*)

    let sem_var (sem:sem_exp_fun) ((var:ide),e1,e2,r,phi) = 
      let ((_,sub1,_,_,_) as ty, phi1) = sem e1 r phi in
      let ((t,sub2,dv,c,f),phi2) = sem e2 (Env.bind(r, var,ty)) phi1 in
	if occur_eenv var phi2 then
	  raise (No_type("sem_var: " ^ var ^ " can't occur in eenv"))
	else
	  let nsub = compose sub1 sub2 in
	    ((apply_subst nsub t, nsub, dv, 
	      apply_subst_cor nsub c, apply_subst_tpred nsub f), 
	     phi2)

    let get_req (v,phi) = 
      let avar = new_annvar() in
      let tylink = tlink(avar) in
      let (t,s,dv,c,f) = 
	type_unify "get_req: no link value" (fresh_type (tylink,No_dval)) v 
      in
      let tyxml = apply_subst s (txml(avar)) in
      let avar1 = apply_subst s avar in
      let pre = extract_eenv (avar1, c, f, A) in
      let nc = filter_cor avar1 c in
      let f1 = f |<- (codomain nc) in
	try
	  let nphi = verify_incl pre phi in
	    ((tyxml, s, No_dval, nc, f1), nphi)
	with _ -> 
	  raise (No_type("get_req: no precondition"))

    let post_req (sem:sem_val_fun) (v,vl, r, phi) = 
      let stringty = fresh_type (tstring,No_dval) in
      let fold_fun type1 v = 
	type_unify "post_req: no string in list of argument" 
	  (sem v r phi) type1
      in
      let(_,s,_,c,f) = List.fold_left fold_fun stringty vl in
      let avar = new_annvar () in
      let tyform = (tform(avar),s,No_dval,c,f) in 
      let (_, nsub, _, c', f') = 
	type_unify "post_req: no form value" tyform v in
      let avar1 = apply_subst nsub avar in
      let pre = extract_eenv (avar1, c', f, A) in
      let nc = filter_cor avar1 c' in
      let nf = f' |<- (codomain nc) in
      let xmlty = txml(avar1) in
	try
	  let nphi = verify_incl pre phi in
	    ((xmlty, nsub, No_dval, nc, nf), nphi)
	with _ -> 
	  raise (No_type("post_req: no preconditions"))

    let patt_type = function
        Punit -> tunit
      | Pzero
      | Psucc(_) -> tinteger
      | Ptext(_)
      | Pelem(_) -> txml(new_annvar())
      | Pstring(_) -> tstring
      | Pnil
      | Pcons(_) -> tlist (new_typevar())
      | Ptuple(l) -> ttuple(List.map (fun x -> new_typevar()) l)

    let smatch (patt, tv, r) =
      let rec make_list e  = function
	  0 -> []
	| n -> e :: (make_list e (n-1))
      in
      let pt = fresh_type ((patt_type patt),Unknown) in
      let (t,sub,dv,c,f) as ty = 
	type_unify "smatch: pattern and value have different type" pt tv in
	match patt with
          Punit
        | Pnil
        | Pzero -> r
        | Psucc(x) -> Env.bind(r, x, ty)
        | Ptext(x) -> Env.bind(r, x, (tstring, sub, No_dval, c, f))
        | Pelem(x,xl) ->
            let r' = Env.bind(r, x, (tstring, sub, No_dval, c, f)) in
              begin
                try
                  Env.bind_list(r', xl, make_list ty (List.length xl))
                with _ -> raise (No_type("smatch: bind_list"))
              end
        | Pstring(cl) ->
            begin
              try
                Env.bind_list(r, cl,
                              make_list ((tinteger, sub, Unknown,c,f)) (List.length cl))
              with _ -> raise (No_type("smatch: bind_list"))
            end
        | Pcons(h, tail) ->
            Env.bind(Env.bind(r, tail, ty), h, (List.hd(get_typevar t), sub,Unknown,c,f))
        | Ptuple(vl) ->
            begin
              try
                Env.bind_list(r, vl, (List.map (fun x -> (x,sub,Unknown,c,f)) (get_typevar t)))
              with _ -> raise (No_type("smatch: bind_list"))
            end

    let switch (sem:sem_exp_fun) (e1, patt, e2, e3, r, phi) = 
      let (tg, phi1) = sem e1 r phi in
      let r' = smatch (patt, tg, r) in
      let (t1, phi2) = sem e2 r' phi1 in 
      let (t2, phi3) = sem e3 r phi1 in
      let ty = type_unify "switch: no unification" t1 t2 in
	(ty, subeffect_eenv phi2 phi3)

    let mark_printer ppf m =
      Format.fprintf ppf "%s" (string_of_mark m)

    let dval_printer ppf d = 
      Format.fprintf ppf "%s" (string_of_dval d)

    let tpred_printer =
        let bind_printer ppf (q,t) =
          Format.fprintf ppf "%s -> %a" q dval_printer t
        in
	  Fun.fun_printer bind_printer 

    let eenv_printer = 
      let bind_printer ppf (q,(d,m)) = 
	Format.fprintf ppf "%s -> (%a,%a)" q 
	  dval_printer d mark_printer m
      in
	Fun.fun_printer bind_printer

    let tipoann_printer ppf v =
      let (t,s,dv,c,f) = v in
	Format.fprintf ppf "@[%a@ %a@ %a@ %a@]"
          tipo_printer  t (*subst_printer s*) dval_printer dv
	  ann_printer c tpred_printer f

    let eval_printer ppf v =
      Format.fprintf ppf "type - :@ %a" tipoann_printer v
  
end

