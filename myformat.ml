let list_printer (element_printer, left_del, right_del, sep) ppf list = 
  let rec f_aux ppf = function
      [] -> ()
    | h::t -> Format.fprintf ppf "%s@ %a%a" sep 	
	element_printer h f_aux t
  in
    if list != [] then
      Format.fprintf ppf "@[%s%a%a%s@]" left_del 
	element_printer (List.hd list) f_aux (List.tl list) right_del
    else
      Format.fprintf ppf "@[%s%s@]" left_del right_del
	
