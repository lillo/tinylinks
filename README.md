# README #

A concrete and an abstract semantics for inferring types and effects of a small fragment of the [Links](http://links-lang.org/) language.

This project is a proof of concept implementing the abstract semantics described in 

- **An Abstract Semantics for Inference of Types and Effects in a Multi-Tier Web Language**, L. Galletta and G. Levi, WWV 2011  

- **An Abstract Interpretation Framework for Type and Effect Systems**, L. Galletta, Fundamenta Informaticae 134 (3-4), 2014. 


## Requirements to build the project #

The project requires 

- [OCaml](http://www.ocaml.org/) compiler

- Make


## Building the project #

To build type in the command line:

```
 $ make all

```

## Running #

The command:

```
 $ tinylinks

```

starts an OCaml toplevel with pre-loaded all the libraries for TinyLinks

### Examples
The file examples.ml contains some examples of programs in abstract syntax tree.


## Content of the repository #

- README.md                                  this file