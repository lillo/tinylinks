OCAMLC=ocamlc
OCAMLMKTOP=ocamlmktop

CALLTRACK=tinylinks
OBJECTS=syntax.cmo myformat.cmo env.cmo tipo.cmo val_dom.cmo cdom.cmo adom.cmo sem.cmo examples.cmo

.PHONY: clean dist-clean

all: calltrack

calltrack: objs
	$(OCAMLMKTOP) $(OBJECTS) -o $(CALLTRACK)

objs: $(OBJECTS)

clean:
	rm -f *.cm[iox] *~

dist-clean: clean
	rm -f $(CALLTRACK)

.SUFFIXES: .ml .cmo

%.cmo : %.ml
	$(OCAMLC) -c $<
