type ide = string
type pred = string

type value = 
    Id of string
  | Unit 
  | Zero 
  | Succ of value
  | String of value list
  | Nil 
  | Cons of value * value
  | Tuple of value list
  | Elem of value * value list
  | Text of value
  | Href of exp
  | Form of ide list * exp
  | Fun of ide * exp

and pattern =
    Punit 
  | Pzero
  | Psucc of ide
  | Pnil 
  | Pcons of ide * ide
  | Ptext of ide
  | Pelem of ide * ide list
  | Ptuple of ide list
  | Pstring of ide list

and exp = 
    Val of value
  | Var of ide * exp * exp
  | Rec of ide * value
  | Plus of exp * exp
  | Minus of exp * exp
  | Times of exp * exp
  | Apply of exp * exp
  | Get of value
  | Post of value list * value
  | Event of pred * value
  | Assert of pred * value
  | Switch of exp * pattern * exp * exp
