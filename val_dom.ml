open Syntax
open Env

module type VALUES = 
  functor (Env: ENV) -> 
sig
  type eval
  type env = (ide, eval) Env.t
  type eenv 
  type mark = E | A | EA

  type sem_val_fun = value -> env -> eenv -> eval
  type sem_exp_fun = exp -> env -> eenv -> (eval * eenv)

  val empty_eenv: eenv

  val make_val: eval * eenv -> (eval * eenv)
  val make_unit: value -> eval
  val make_int: sem_val_fun -> (value * env * eenv) -> eval
  val make_string: sem_val_fun -> (value * env * eenv) -> eval
  val make_tuple: sem_val_fun -> (value * env * eenv) -> eval
  val make_xml: sem_val_fun -> (value * env * eenv) -> eval
  val make_list: sem_val_fun -> (value * env * eenv) -> eval
  val make_fun: sem_exp_fun -> (value * env * eenv) -> eval
  val make_xml_fun: sem_exp_fun -> (value * env * eenv) -> eval
  val apply_fun:  eval * eval * eenv -> (eval * eenv)
  val make_fun_rec: sem_exp_fun -> ide * value * env * eenv -> eval 
  val sem_op: sem_exp_fun -> (int -> int -> int) -> exp * exp * env * eenv -> (eval * eenv)
  val sem_var: sem_exp_fun -> ide * exp * exp * env * eenv -> (eval * eenv)
  val event_assert: bool -> (pred * eval * eenv) -> (eval * eenv)
  val switch: sem_exp_fun -> exp * pattern * exp * exp * env * eenv -> (eval * eenv)
  val get_req: eval * eenv -> (eval * eenv)
  val post_req: sem_val_fun -> eval * value list * env * eenv -> (eval * eenv)
  val eval_printer: Format.formatter -> eval -> unit
  val eenv_printer: Format.formatter -> eenv -> unit
end
