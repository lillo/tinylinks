open Val_dom
open Env
open Syntax

module ConcreteDomain : VALUES = 
  functor (Env: ENV) ->
struct
  exception Wrong_value of string
  exception Fail_match
  
(*  type dval = 
      Dint of int 
    | No_dval *)
  type dval = 
      Dint of int
    | Var
    | No_dval
    | Unknown
    | Dval



  type mark = E | A | EA
  type eenv = (pred, (dval * mark)) Env.t

  type eval = 
        Unit_val
      | Int_val of int
      | Nil_val
      | Cons_val of eval * eval
      | String_val of eval list
      | Tuple_val of eval list
      | Text_val of eval
      | Elem_val of eval * eval list
      | Href_val of ehref
      | Form_val of ide list * eform
      | Fun_val of efun

  and ehref = eenv -> eval * eenv
  and eform = eenv -> eval list -> eval * eenv
  and efun = eenv -> eval -> eval * eenv

  type env = (ide, eval) Env.t    

  type sem_val_fun = value -> env -> eenv -> eval
  type sem_exp_fun = exp -> env -> eenv -> (eval * eenv)

  let empty_eenv : eenv = Env.empty_env
	
  let eval_to_dval = function
      Int_val(n) -> Dint(n)
    | _ -> No_dval

  let make_val (ev, phi) = (ev, phi)

  let make_unit = function
      Unit -> Unit_val
    | _ -> raise (Wrong_value("make_unit: no unit argument"))
	

  let rec make_int sem_val (v, r, f) =
    match v with
        Zero -> Int_val(0)
      | Succ(p) ->
          begin
            match (sem_val p r f) with
                Int_val(n) -> Int_val(n + 1)
              | _ -> raise (Wrong_value("make_int: no int argument"))
          end
      | _ -> raise (Wrong_value("make_int: no int argument"))

	  
  let rec make_string (sem_val: sem_val_fun) ((v: value), (r: env), (f: eenv)) =
    match v with
        String(vl) ->
          begin
            let map_fun v =
              match (sem_val v r f) with
                  Int_val(_) as c -> c
                  | _ -> raise (Wrong_value("make_string: no int argument"))
            in
              String_val(List.map map_fun vl)
          end
      | _ -> raise (Wrong_value("make_string: no string argument"))


  let make_tuple (sem_val: sem_val_fun) ((v: value), (r: env), (f: eenv)) =
    match v with
        Tuple(vl) ->
          begin
            let map_fun x = sem_val x r f in
              Tuple_val(List.map map_fun vl)
          end
      | _ -> raise (Wrong_value("make_tuple: no tuple argument"))


  let make_xml (sem_val: sem_val_fun) ((v: value), (r: env), (f: eenv)) =
    match v with
        Text(v') ->
          begin
            match (sem_val v' r f) with
                String_val(_) as s -> Text_val(s)
              | _ -> raise (Wrong_value("make_xml: no string argument in text element"))
          end
      | Elem(v', vl) ->
          begin
            let map_fun x =
              begin
                match (sem_val x r f) with
                    Text_val(_)
                  | Elem_val(_)
                  | Href_val(_)
                  | Form_val(_) as xml_val ->
                      xml_val
                  |_ -> raise (Wrong_value("make_xml: no xml argument in list of elem"))
              end
            in
              match (sem_val v' r f) with
                  String_val(_) as s ->
                    Elem_val(s, List.map map_fun vl)
                | _ -> raise
                    (Wrong_value("make_xml: no string argument in first element of elem"))
          end
      | _ -> raise (Wrong_value("make_xml: no xml argument"))

  let rec make_list (sem_val: sem_val_fun) ((v: value), (r: env), (f: eenv)) =
    match v with
        Nil -> Nil_val
      | Cons(head, tail) ->
          begin
            let h = sem_val head r f in
              match (sem_val tail r f) with
                  Cons_val(_)
                | Nil_val as t -> Cons_val(h,t)
                |_ -> raise (Wrong_value("make_list: no a list argument"))
          end
      | _ -> raise (Wrong_value("make_list: no a list argument"))
	  
  let make_fun (sem_exp: sem_exp_fun )((v: value), (r: env), (f':eenv)) =
    match v with
        Fun(var, e) ->
          Fun_val(function f -> function v' -> sem_exp e (Env.bind(r, var, v')) f)
      | _ -> raise (Wrong_value("make_fun: no xml argument"))

  let apply_fun ((fv: eval), (p: eval), (f: eenv)) =
    match fv with
        Fun_val(lam) ->
          lam f p
      | _ -> raise (Wrong_value("apply_fun: no functional object"))

  let make_fun_rec (sem_exp: sem_exp_fun) (var, v, r, f) =
    match v with
        Fun(fp,e) ->
          let rec functional ff phi fa =
            let r' = Env.bind(Env.bind(r, fp,fa), var, Fun_val(ff)) in
              sem_exp e r' phi
          in
          let rec fix phi' x = functional fix phi' x in
            Fun_val(fix)
      | _ -> raise (Wrong_value("No lambda-abstraction"))
	  
  let make_xml_fun (sem_exp: sem_exp_fun) ((v: value), (r:env), (f':eenv)) =
    match v with
        Href(e) ->
          Href_val(function f -> sem_exp e r f)
      | Form(varl, e)->
          Form_val(varl,function f -> function vl -> sem_exp e (Env.bind_list(r, varl, vl)) f)
      | _ -> raise (Wrong_value("make_xml_fun: no web closure argument"))

  let int_op op (n1,n2) =
    match (n1,n2) with
        (Int_val(a),Int_val(b)) -> Int_val(op a b)
      | _ -> raise (Wrong_value("int_op: no integers argument"))

  let sem_op (sem_exp: sem_exp_fun) op (e1,e2,r,f) =
    let (v1, f1) = sem_exp e1 r f in
    let (v2, f2) = sem_exp e2 r f1 in
      (int_op op (v1,v2), f2)

  let compare = function
      (Dint(n), Dint(n')) when n = n' -> Unit_val
    | _ -> raise (Wrong_value("compare: different value"))
	
  let event_assert event (q,v,f) =
    match eval_to_dval v with
	Dint(n) as dv -> 
	  if event then
	    (Unit_val, Env.bind(f, q, (dv, E)))
	  else
	    let va = fst(Env.apply_env(f,q)) in
	      (compare(dv, va), Env.bind(f, q, (dv, EA)))
      | _ -> raise (Wrong_value("event_assert: no dval value"))
      
  let smatch (patt, v, r) =
    match (patt, v) with
        (Punit, Unit_val) -> r
      | (Pzero, Int_val(0)) -> r
      | (Psucc(p), Int_val(n)) when n > 0 ->
          Env.bind(r, p, (Int_val(n - 1)))
      | (Ptext(p), Text_val(s)) ->
          Env.bind(r, p, s)
      | (Pelem(x,xl), Elem_val(t,tl)) ->
          begin
            try
              Env.bind_list(Env.bind(r, x, t), xl, tl)
            with _ -> raise Fail_match
          end
      | (Ptuple(xl), Tuple_val(vl)) ->
          begin
            try
              Env.bind_list(r, xl, vl)
            with _ -> raise Fail_match
          end
      | (Pstring(xl), String_val(vl)) ->
          begin
            try
              Env.bind_list(r, xl, vl)
            with _ -> raise Fail_match
          end
      | (Pnil, Nil_val) -> r
      | (Pcons(h,t), Cons_val(v1, v2)) ->
          Env.bind(Env.bind(r, h, v1), t, v2)
      | _ -> raise Fail_match

  let switch (sem_exp: sem_exp_fun)(e1, patt, e2, e3, r, f) =
    let (v1, f1) = sem_exp e1 r f in
      try
        let r' = smatch(patt,v1, r) in
          sem_exp e2 r' f1
      with Fail_match ->
        sem_exp e3 r f

  let sem_var (sem_exp: sem_exp_fun)(var, e1, e2, r, f) = 
    let (v, f') = sem_exp e1 r f in
      sem_exp e2 (Env.bind(r, var, v)) f'
	  
  let get_req (v,f) =
    match v with
        Href_val(e) -> e f
      | _ -> raise (Wrong_value("get_req: no link values"))

  let post_req (sem_val: sem_val_fun) (v, vl, r, f) =
    match v with
        Form_val(_,e) ->
          begin
            let map_fun x =
              match (sem_val x r f) with
                  String_val(_) as s -> s
                | _ -> raise (Wrong_value("post_req: argument must be string value"))
            in
              e f (List.map map_fun vl)
          end
      | _ -> raise (Wrong_value("post_req: no"))

  let rec eval_printer ppf = function
      Unit_val -> Format.fprintf ppf "@[<2>()@]"
    | Int_val(n) -> Format.fprintf ppf "@[<2>%d@]" n
    | Nil_val -> Format.fprintf ppf "@[<2>[]@]"
    | Cons_val(head,tail) ->
        Format.fprintf ppf "@[<2>%a::%a@]" eval_printer head eval_printer tail
    | Href_val(_) -> Format.fprintf ppf "@[<2>Href<fun>@]"
    | Form_val(_)-> Format.fprintf ppf "@[<2>Form<fun>@]"
    | Fun_val(_) -> Format.fprintf ppf "@[<2>Fun<fun>@]"
    | String_val(vl) -> Format.fprintf ppf "@[<2>String@]"
    | Text_val(v) -> Format.fprintf ppf "@[<2>Text(%a)@]" eval_printer v
    | Elem_val(v, vl) -> Format.fprintf ppf "@[<2>Elem(%a, %a)@]"
        eval_printer v l_eval_printer vl
    | Tuple_val(vl) -> Format.fprintf ppf "@[<2>Tuple(%a)@]"
        l_eval_printer vl
  and l_eval_printer ppf l =
    if (List.length l) > 0 then
      let rec f_aux ppf = function
          [] -> ()
        |h::t -> Format.fprintf ppf "@[<2>,%a%a@]" eval_printer h f_aux t
      in
          Format.fprintf ppf "@[<2>%a%a@]" eval_printer (List.hd l) f_aux (List.tl l)
    else
      ()
	
  let dval_printer ppf dv = 
    let a = match dv  with 
	Dint(i) -> string_of_int i
      | _ -> "No_dvar"
    in
      Format.fprintf ppf "%s" a

  let mark_printer ppf m =
    let str_rep =
      match m with
          E -> "E"
        | A -> "A"
        | EA -> "EA"
    in
      Format.fprintf ppf "%s" str_rep
	
  let eenv_printer =
    let pred_printer ppf p =
      Format.fprintf ppf "%s" p
      in
    let f_aux ppf (v,m) =
      Format.fprintf ppf "(%a,%a)"
        dval_printer v mark_printer m
    in
      Env.printer (pred_printer, f_aux)  
end
