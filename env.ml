module type ENV = 
sig
  type ('a,'b) t
  val empty_env : ('a,'b) t
  val bind : (('a,'b) t * 'a * 'b) -> ('a,'b) t
  val bind_list : (('a,'b) t * 'a list * 'b list) -> ('a,'b) t
  val apply_env : (('a,'b) t * 'a) -> 'b
  val printer: ((Format.formatter -> 'a -> unit) * (Format.formatter -> 'b -> unit)) ->
		  Format.formatter -> ('a,'b) t -> unit
end

module FunEnv : ENV = 
struct
  type ('a,'b) t = 'a -> 'b

  let empty_env = 
    fun x -> failwith "apply_env: Unbound"

  let bind (f, id, v) = 
    fun x -> if x = id then v else f x

  let bind_list (f, id_list, v_list) = 
    let f_aux f x v = bind(f, x, v) in
      List.fold_left2 f_aux f id_list v_list

  let apply_env (f, id) =
    f id

  let printer (a_printer,b_printer) ppf f = 
    Format.fprintf ppf "@[Env<fun>@]"
end

module ListEnv : ENV = 
struct
  type ('a,'b) t = ('a * 'b) list

  let empty_env =
    ([] : ('a,'b) t)

  let bind (f, id, v) = 
    (id,v) :: (List.remove_assoc id f)

  let bind_list (f, id_list, v_list) = 
    let f_aux f x v = bind(f, x, v) in
      List.fold_left2 f_aux f id_list v_list

  let apply_env (f, id) =
    try
      List.assoc id f
    with Not_found -> failwith "apply_env: Unbound"

  let printer (a_printer, b_printer) = 
    let bind_printer ppf (a,b) = 
      Format.fprintf ppf "%a -> %a" 
	a_printer a b_printer b 
    in
      Myformat.list_printer(bind_printer, "{", "}", ",")
end
