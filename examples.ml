open Syntax

let rec int_to_num = function
    0 -> Zero
  | n -> Succ(int_to_num (n-1))


(* Fattoriale 
  fun fact(n){
    switch(n){
      Zero -> 1
      _ -> n * (fact (n - 1))
    }
  }

*)

let fact = 
  Rec("fact", Fun("n",
		  Switch(Val(Id("n")),
			 Pzero,
			 Val(Succ(Zero)),
			 Times(Val(Id("n")),
			       Apply(Val(Id("fact")), 
				     Minus(Val(Id("n")), Val(Succ(Zero))))
			      )
			)
		 )
     )


(* Fattoriale di 5 
  fact 5
*)

let fact5 = 
  Apply(fact, Val(int_to_num 5))

(* Calcolo esponenziale 
  fun exp(b,n){
    switch(n){
      Zero -> 0
      _ -> b * (exp b (n-1))
    }
  }
*)

let exp_fun = 
  Rec("exp", 
      Fun("b", Val(
	    Fun("n", Switch(Val(Id("n")),
			    Pzero,
			    Val(Succ(Zero)),
			    Times(Val(Id("b")), 
				  Apply(
				    Apply(Val(Id("exp")), Val(Id("b"))),
				    Minus(Val(Id("n")), Val(Succ(Zero)))
				  )
				 )
			   )
	       )
	  )
	 )
     )

(* Calcolo 2^5 
  exp 2 5
*)

let exp25 = 
  Apply(Apply(exp_fun, Val(int_to_num 2)), Val(int_to_num 5))


(* Funzione identita' 
  fun id(x) { x }
*)

let i = 
  Val(Fun("x", Val(Id("x"))))

(* Identita' di 10 
  id 10
*)

let i10 = 
  Apply(i, Val(int_to_num 10))

(* Lista di interi  [1;2;3;4;5] *)

let list15 = 
  Val(Cons(int_to_num 1, 
       Cons(int_to_num 2,
	    Cons(int_to_num 3,
		 Cons(int_to_num 4,
		      Cons(int_to_num 5, Nil)
		     )
		)
	   )
	  ))

(* Funzione map 
  
  fun map(f,l){
    switch(l){
      Cons(h,t) -> var hf = f h;
		   var tl = map f t;
		   Cons(hf, tl)
      _ -> Nil
    }
  }
  
*)

let map_fun = 
  Rec("map",Fun("f",Val(
	    Fun("l",
		Switch(Val(Id("l")),
		       Pcons("h","t"),
		       Var("hf", 
			   Apply(Val(Id("f")), Val(Id("h"))),
			   Var("tl", 
			       Apply(Apply(Val(Id("map")), Val(Id("f"))), Val(Id("t"))),
			       Val(Cons(Id("hf"),Id("tl")))
			      )),
		       Val(Nil))
	       )
		)
	       )
     )

(* Funzione raddoppia 
  fun r(n) { n * 2 }
*)

let x2 = 
  Val(Fun("n", Times(Val(Id("n")), Val(int_to_num 2))))

(* Applicazione di map con x2 su list15 

  map r [1;2;3;4;5]

*)

let map_list15 = 
  Apply(Apply(map_fun, x2), list15)
    

(* 
  Funzione somma 
  fun somma(n1, n2) { n1 + n2 } 
*)

let somma = 
  Val(Fun("n1", 
	  Val(Fun("n2",
		  Plus(Val(Id("n1")), Val(Id("n2")))
		 )
	     )
	 )
     )

(* Funzione somma_coppia 
  fun somma_coppia (pair){
    switch(pair){
      Tuple(x1,x2) -> somma x1 x2
      _ -> 0
    }
  }
*)

let somma_coppia = 
  Val(Fun("pair",
	  Switch(
	    Val(Id("pair")),
	    Ptuple(["x1";"x2"]),
	    Apply(Apply(somma, Val(Id("x1"))), Val(Id("x2"))),
	    Val(Zero)
	  )
	 )
     )

(* 
   Applica somma_coppia ad una tripla 
   Deve dare errore nel pattern matching

   var tripla = Tuple(5, 6, 7);
    somma_coppia tripla
*)
let app_somma_coppia1 = 
  Var("tripla", 
      Val(Tuple([int_to_num 5; int_to_num 6; int_to_num 7])),
      Apply(somma_coppia, Val(Id("tripla")))
     )

(*
  Applica somma_coppia in modo corretto
  il risultato deve essere 7

  var tripla = Tuple(3,4);
    somma_coppia tripla
*)
let app_somma_coppia2 = 
  Var("tripla", 
      Val(Tuple([int_to_num 3; int_to_num 4])),
      Apply(somma_coppia, Val(Id("tripla")))
     )   

(* Funzione fold 
  
  fun fold(f, v, l){
    switch(l){
      Cons(h,t) -> fold f (f v h) t
      _ -> v
    }
  }
  
*)

let fold_fun =
  Rec("fold", 
      Fun("f", 
	  Val(Fun("v", 
		  Val(Fun("l",
			  Switch(Val(Id("l")),
				 Pcons("h","t"),
				 Apply(
				   Apply(
				     Apply(Val(Id("fold")), Val(Id("f"))),
				     Apply(
				       Apply(Val(Id("f")), Val(Id("v"))),
				       Val(Id("h"))
				     )
				   ),
				   Val(Id("t"))
				 ),
				 Val(Id("v"))
				)
			 )
		     )
		 )
	     )
	 )
     )


(* Applicazione 

fold somma 0 [1;2;3;4;5] 

*)

let fold_somma_list15 = 
  Apply(
    Apply(
      Apply(fold_fun, somma),
      Val(Zero)
    ),
    list15
  )

(* Funzione somma 5 
  var somma_5 = somma 5
*)

let somma_5 = 
  Apply(somma, Val(int_to_num 5))

(* Applicazione map (somma 5) list15 
  
  map somma_5 [1;2;3;4;5]
  
*)

let map_somma5_list15 =
  Apply(
    Apply(map_fun, somma_5),
    list15
  )

(* Esempi con event e assert *)

(*
  Funzione che richiede evento q di tipo int

  fun req_q(x) {
    var _ = assert q(x);
      x + 2
  }
*)

let req_q = 
  Val(Fun("x",
	  Var("_",
	      Assert("q", Id("x")),
	      Plus(Val(Id("x")), Val(int_to_num 2))
	     )
	 )
     )

(*
  Deve dare errore perchè l'evento q è generato con valore
  10, ma la funzione req_q è invocata con valore 5

  fun usa_req1(y){
    var f = req_q;
    var _ = event q(10);
      f 5
  }
*)
let usa_req1 = 
  Val(Fun("y",
	  Var("f", 
	      req_q,
	      Var("_", 
		  Event("q", int_to_num 10),
		  Apply(Val(Id("f")), Val(int_to_num 5))
		 )
	     )
	 )
     )

(*
  Come la precedente ma funziona

  fun usa_req1(y){
    var f = req_q;
    var _ = event q(10);
      f 10
  }
*)
let usa_req2 = 
  Val(Fun("y",
	  Var("f", 
	      req_q,
	      Var("_", 
		  Event("q", int_to_num 10),
		  Apply(Val(Id("f")), Val(int_to_num 10))
		 )
	     )
	 )
     )

(*
  Una semplice funzione ricorsiva 
  che ad ogni passo verifica l'evento q.

  fun asc(x){
    var _ = assert q(10);
      switch(x){
	Succ(p) -> asc p
	_ -> Unit
      }
  }
*)
let asc_rec_fun = 
  Rec("asc",
      Fun("x",
	  Var("_", 
	      Assert("q", int_to_num 10),
	      Switch(
		Val(Id("x")),
		Psucc("p"),
		Apply(Val(Id("asc")), Val(Id("p"))),
		Val(Unit)
	      )
	     )
	 )
     )
	
(*
  Chiama asc_rec_fun con il valore passato come
  parametro. Da un errore di tipo perche' si applica
  asc senza precondizioni soddisfatte

  fun p_fun(x){
    asc x
  }
*)
	
let p_fun = 
  Val(Fun("x",
	  Var("asc",
	      asc_rec_fun,
	      Apply(Val(Id("asc")), Val(Id("x")))
	     )
	 )
     )

(*
  Chiama asc_rec_fun con il valore passato come
  parametro. Da notare che viene generato un evento
  con un valore diverso da quello che deve essere verificato.
  Il Typecheck da un errore di tipo

  fun p_fun1(x){
    var _ = event q(5);
    asc x
  }
*)

let p_fun1 = 
  Val(Fun("x",
	  Var("_", 
	      Event("q", int_to_num 5),
	      Var("asc",
		  asc_rec_fun,
		  Apply(Val(Id("asc")), Val(Id("x")))
		 )
	     )
	 )
     )

(*
  Come quella precedente ma funziona

  fun p_fun1(x){
    var _ = event q(10);
    asc x
  }
*)
let p_fun11 = 
  Val(Fun("x",
	  Var("_", 
	      Event("q", int_to_num 10),
	      Var("asc",
		  asc_rec_fun,
		  Apply(Val(Id("asc")), Val(Id("x")))
		 )
	     )
	 )
     )

(*
  Come p_fun1 ma non invoca al suo interno
  asc, ma la restituisce: da un errore perchè
  la funzione tpred assume il top di dval, il che
  non è ammesso

  fun p_fun2(x){
    var _ = event q(5);
    asc 
  }
*)

let p_fun2 = 
  Val(Fun("x",
	  Var("_", 
	      Event("q", int_to_num 5),
	      Var("asc",
		  asc_rec_fun,
		  Val(Id("asc"))
		 )
	     )
	 )
     )
(*
  Come quella precedente ma funziona
  fun p_fun3(x){
    var _ = event q(10);
    asc 
  }
*)
let p_fun3 = 
  Val(Fun("x",
	  Var("_", 
	      Event("q", int_to_num 10),
	      Var("asc",
		  asc_rec_fun,
		  Val(Id("asc"))
		 )
	     )
	 )
     )

(*
  Applica p_fun1 a 5:
  da un errore di tipo come p_fun1
  
  p_fun1 5
*)

let apply_p_fun1 = 
  Var("p_fun1",
      p_fun1,
      Apply(Val(Id("p_fun1")), Val(int_to_num 5))
     )

(*
  Deve dare errore di tipo perché i due rami dello
  switch devono generare lo stesso evento

  var _ = 
    switch(5){
      Zero -> event q(10)
      _ -> Unit
    };
      asc 5
*)
let unsafe1 = 
  Var("asc", 
      asc_rec_fun,
      Var("_",
	  Switch(
	    Val(int_to_num 5),
	    Pzero,
	    Event("q", int_to_num 10),
	    Val(Unit)
	  ),
	  Apply(Val(Id("asc")), Val(int_to_num 5))
	 )
     )

(*
  Come quella precedente, ma passa il typecheck

  var _ = 
    switch(5){
      Zero -> event q(10)
      _ -> event q(10)
    };
      asc 5
*)
let unsafe2 = 
  Var("asc", 
      asc_rec_fun,
      Var("_",
	  Switch(
	    Val(int_to_num 5),
	    Pzero,
	    Event("q", int_to_num 10),
	    Event("q", int_to_num 10)
	  ),
	  Apply(Val(Id("asc")), Val(int_to_num 5))
	 )
     )

(*
  Funzione che genera un evento con il valore del suo parametro (value1),
  e che poi invoca una funzione interna che verifica lo stesso
  evento con il suo rispettivo parametro (value2). Deve passare il typecheck

  fun ppp(value1){
    fun f(value2){
      var _ = assert q(value2);
	Unit
    };
    var _ = event q(value1);
      f value1
  }
*)
let ppp = 
  Val(Fun("value1",
	  Var("fun", 
	      Val(Fun("value2",
		      Var("_",
			  Assert("q",Id("value2")),
			  Val(Unit)
			 )
		     )
		 ),
	      Var("_",
		  Event("q",Id("value1")),
		  Apply(Val(Id("fun")),Val(Id("value1")))
		 )
	     )
	 )
     )
	

(*
  Esempio che genera un evento nella guardia dello switch

  switch(var _ = event q(5); 0){
    Zero -> 10
    _ -> 5
  }
*)	       

let guardia_switch1 = 
  Switch(
    Var("_",Event("q",int_to_num 5), Val(Zero)),
    Pzero,
    Val(int_to_num 10),
    Val(int_to_num 5)
  )

(*
  Esempio che richiede un evento nella guardia dello switch

  switch(var _ = assert q(5); 0){
    Zero -> 10
    _ -> 5
  }
*)	       

let guardia_switch2 = 
  Switch(
    Var("_",Assert("q",int_to_num 5), Val(Zero)),
    Pzero,
    Val(int_to_num 10),
    Val(int_to_num 5)
  )

(*
  Esempio di evento "locale"

  fun local_event1(x){
    var _ = event q(1);
      assert q(5)
  }
*)
let local_event1 = 
  Val(Fun("x",
	  Var("_", Event("q",int_to_num 1),
	      Assert("q",int_to_num 5)
	     )
	 )
     )

(*
  Esempio di evento "locale" dove prima
  asserisco e poi genero: questo deve dare
  un errore di tipo

    fun local_event2(x){
    var _ = assert q(1);
      event q(5)
  }
*)
let local_event2 = 
  Val(Fun("x",
	  Var("_", Assert("q",int_to_num 1),
	      Event("q",int_to_num 5)
	     )
	 )
     )

(*
  Esempio di evento asserito due volte con 
  valori diversi: deve dare errore (caso var)
*)
let var_double_assert =
  Var("_", 
      Assert("q", int_to_num 2),
      Assert("q", int_to_num 3)
     )

(*
  Esempio di evento asserito due volte con 
  valori diversi: deve dare errore (caso switch)
*)
let switch_double_assert = 
  Switch(Assert("q", int_to_num 2),
	 Punit,
	 Assert("q", int_to_num 3),
	 Assert("q", int_to_num 4)
	)

(* Il rebinding deve essere vietato perché può portare
  a situazioni unsafe *)
(*
  Esempio di rebinding di un evento
*)
let rebinding = 
  Var("dummy1",
      Event("q", int_to_num 5),
      Var("asc", 
	  asc_rec_fun,
	  Var("dummy2",
	      Event("q", int_to_num 10),
	      Apply(Val(Id("asc")), Val(int_to_num 2))
	     )
	 )
     )
(*
  Altro esempio di rebinding
*)
let rebinding2 = 
  Var("cl", 
      Apply(p_fun3,Val(int_to_num 2)),
      Var("_",
	  Event("q", int_to_num 0),
	  Apply(Val(Id("cl")), Val(int_to_num 3))
	 )
     )
      

(* Esempi presi da aritcolo di Gordon *)

(* buy_fun 
  fun buy(value, dbpass) {
    var _ = assert PriceIs(value);
      Text("a")
  }
*)

let buy_fun =
  Val(Fun("value", 
	  Val(Fun("dbpass",
		  Var("_", 
		      Assert("PriceIs",Id("value")),
		      Val(Text(String([int_to_num 2; int_to_num 1])))
		     )
		 )
	     )
	 )
     )

(* 
   Applicazione buy_fun 
   senza evento PriceIs generato
   TypeError

   buy 5 "a"
*)

let simple_app_buy_fun1 =
  Var("buy", 
      buy_fun,
      Apply(Apply(Val(Id("buy")), Val(int_to_num 5)),Val(String([int_to_num 0])))
     )

(*
  Come sopra eccetto che viene fatta una applicazione parziale:
  viene fissato il primo argomento e viene "specializzato"  
  l'evento richiesto 

  buy 5
*)

let simple_app_buy_fun2 =
  Var("buy", 
      buy_fun,
      Apply(Val(Id("buy")), Val(int_to_num 5))
     )

(*
  sellAt_fun
  Richiede di essere valutata in un ambiente
  che contiene il legame tra l'identificatore
  buy e la funzione buy_fun

  fun sellAt(price){
    var dbpass = "pass";
    var _ = event PriceIs(price);
    <form l:onsubmit="{buy price dbapss}">
    <button type="submit">submit</button>
    </form>
  }

 *)

let sellAt_fun = 
  Val(Fun("price",
	  Var("dbpass", 
	      Val(String([int_to_num 1; int_to_num 2])),
	      Var("_",
		  Event("PriceIs", Id("price")),
		  Val(
		    Form([],
			 Apply(
			   Apply(Val(Id("buy")), Val(Id("price"))),
			   Val(Id("dbpass"))
			 )
			)
		  )
		 )
	     )
	 )
     )

(*
  Data Integrity with Assertions: Sale
  sellAt 5
*)

let program_sale = 
  Var("buy", 
      buy_fun,
      Var("sellAt",
	  sellAt_fun,
	  Apply(Val(Id("sellAt")), Val(int_to_num 5))
	 )
     )

(*
  Control Integrity with Assertions: Unreachable

  fun f(y) { 
    Text("a") 
  }
*)

let f_fun = 
  Val(Fun("y", 
	  Val(Text(
		String([int_to_num 5])
	      )
	     )
	 )
     )

(*
  fun g(y){
    var _ = assert Unreachable(1);
      Text("a")
  }
*)

let g_fun = 
  Val(Fun("y",
	  Var("_", 
	      Assert("Unreachable", int_to_num 1),
	      Val(Text(
		    String([int_to_num 2])
		  )
		 )
	     )
	 )
     )
(*
  fun page(x) {
    fun f(y) { Text("a") };
    fun g(y){ var _ = assert Unreachable(1); Text("a") };
    <a l:href="{var w = g; f ()}">link</a>
  }
*)
let page_fun = 
  Val(Fun("x",
	  Var("f", 
	      f_fun,
	      Var("g", 
		  g_fun ,
		  Val(Href(
		    Var("w", 
			Val(Id("g")),
			Apply(Val(Id("f")), Val(Unit))
		       ) )
		  )
		 )
	     )
	 )
     )

(*
  page 5
*)
let unreachable_program =
  Var("page", 
      page_fun,
      Apply(Val(Id("page")), Val(int_to_num 5))
     )


(*
  Identica a page_fun tranne che viene invocata
  la funzione g: deve dare un errore di tipo

  fun page2(x) {
    fun f(y) { Text("a") };
    fun g(y){ var _ = assert Unreachable(1); Text("a") };
    <a l:href="{var w = g; f ()}">link</a>
  }
*)

let page_fun1 = 
  Val(Fun("x",
	  Var("f", 
	      f_fun,
	      Var("g", 
		  g_fun ,
		  Val(Href(
		    Var("w", 
			Val(Id("g")),
			Apply(Val(Id("w")), Val(Unit))
		       ) )
		  )
		 )
	     )
	 )
     )
(*
  page2 5
*)
let unreachable_program1 =
  Var("page", 
      page_fun1,
      Apply(Val(Id("page")), Val(int_to_num 5))
     )


