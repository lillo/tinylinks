open Syntax
open Env
open Val_dom
open Cdom
open Adom

module Semantics = (* (Env: ENV) (Values: VALUES) = *)
  functor (Env : ENV) ->
  functor (Values: VALUES) ->
struct
  module V = Values(Env)
  module E = Env

  open Env
  open V

  let rec sem_val (v: value) (r: env) (f: eenv) = 
    match v with 
	Id(var) -> apply_env (r, var)
      | Unit -> make_unit v 
      | Zero 
      | Succ(_) -> make_int sem_val (v, r, f)
      | String(_) -> make_string sem_val (v, r, f)
      | Tuple(_) -> make_tuple sem_val (v, r, f)
      | Text(_) 
      | Elem(_) -> make_xml sem_val (v, r, f)
      | Href(_)
      | Form(_) -> make_xml_fun sem_exp (v, r, f)
      | Fun(_) -> make_fun sem_exp (v,r,f)
      | Nil 
      | Cons(_) -> make_list sem_val (v,r,f)

  and sem_exp (e: exp) (r: env) (f: eenv) = 
    match e with
	Val(v) -> make_val (sem_val v r f, f)
      | Var(var, e1, e2) ->
	  sem_var sem_exp (var, e1, e2, r, f)
      | Apply(e1, e2) ->
	  let (fv, f1) = sem_exp e1 r f in
	  let (p, f2) = sem_exp e2 r f1 in
	    apply_fun (fv, p, f2)
      | Plus(e1,e2) -> 
	  sem_op sem_exp (+) (e1,e2,r,f)
      | Minus(e1,e2) -> 
	  sem_op sem_exp (-) (e1,e2,r,f)
      | Times(e1,e2) -> 
	  sem_op sem_exp ( * ) (e1,e2,r,f)
      | Event(q, v) ->
	  event_assert true (q, sem_val v r f, f)
      | Assert(q, v) ->
	  event_assert false (q, sem_val v r f, f)
      | Rec(var, v) ->
	  (make_fun_rec sem_exp (var, v, r, f), f)
      | Switch(e1,patt,e2,e3) ->
	  switch sem_exp (e1,patt,e2,e3, r, f)
      | Get(v) -> 
	  get_req ((sem_val v r f),f)
      | Post(vl, v) ->
	  post_req sem_val ((sem_val v r f), vl, r, f)

  let eval_printer = V.eval_printer
  let eenv_printer = V.eenv_printer
  let env_printer = Env.printer

end

module ConcreteSemantics = Semantics(ListEnv)(ConcreteDomain)
module AbstractSemantics = Semantics(ListEnv)(AbstractDomain) 
let csem = ConcreteSemantics.sem_exp
let asem = AbstractSemantics.sem_exp 
let empty_env = ConcreteSemantics.E.empty_env
let empty_eenv = ConcreteSemantics.V.empty_eenv
let empty_aenv = AbstractSemantics.E.empty_env
let empty_aeenv = AbstractSemantics.V.empty_eenv 




